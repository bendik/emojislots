# BYO Emojislotmachine! (WIP)

Written with `redux` and `vdux`

### Requirements
* Node.js >= 4
* `npm install -d`
* `make development`

#### Features
* [x] Spin the wheels
* [x] Edit the emojis
* [ ] Add animation for a winning combination
* [ ] Share your configured slotmachine

### Known issues
* `emoji-regex` does some emoji-convertions
* ios safari rendering
