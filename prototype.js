// UNUSED first initial prototype for this slotmachine
import emojiRegex from 'emoji-regex'

const emojiStringToArray = function (str) {
  let split = str.match(emojiRegex());
  let arr = [];
  if(split){
    for (var i=0; i<split.length; i++) {
      let char = split[i]
      if (char !== "") {
        arr.push(char);
      }
    }
  }
  return arr;
}

export default (emojis) => {
  let state = {
    emojis,
    spinDuration: 4,
    wheels: [],
    slideSeries: [3,3,3,3,4,4,2]
  }
  return Object.assign(
    {},
    play(state),
    init(state)
  )
}

const addSlide = (state, emoji, el) => {
  let slideEl = document.createElement('slide')
  slideEl.style.transform = 'rotateX(' + (el.childNodes.length * state.slideAngle) + 'deg) translateZ(80vmax) scale(1.5)';
  slideEl.classList.add('emoji')
  slideEl.textContent = emoji;
  el.appendChild(slideEl);
}

const addWheel = (state, el) => {
  let wheelEl = document.createElement('wheel')
  let wheel = {}
  state.wheels.push(wheel)
  wheel.rotation = 0;
  wheel.spinDelay = (state.wheels.length * .25)
  wheel.spinDuration = state.spinDuration + wheel.spinDelay
  wheelEl.style['transform-duration'] = wheel.spinDuration +'s'
  wheelEl.style['transform-delay'] = wheel.spinDelay + 's'

  let slideCount = 0;
  // randomize emoji placements
  let emojis = state.emojis.sort((a,b) => parseInt(Math.random()*10)%2)
  emojis.forEach((emoji, i) => {
    var emojiCount = state.slideSeries[i]
    while (emojiCount > 0){
      addSlide(state, emoji, wheelEl)
      emojiCount--
      slideCount++
    }
  })
  el.appendChild(wheelEl)
}

const init = (state) => ({
  init: () => {
    let el = document.createElement('machine')
    state.totalSlides = state.slideSeries.reduce((a,b)=> a + b, 0)
    state.slideAngle = 360 / state.totalSlides;
    while (state.wheels.length < 3){
      addWheel(state, el)
    }
    document.body.appendChild(el)
    return state
  }
})

const play = (state) => ({
  play: () => {
    var wheels = document.querySelectorAll('wheel')
    let totalSlides = state.slideSeries.reduce((a,b)=> a + b, 0)
    for (var i = 0; i < wheels.length; i++){
      var degreesToMove = Math.floor((Math.random() * 1.5 * state.totalSlides) * 2 + state.totalSlides) * state.slideAngle;
      wheels[i].rotation -= degreesToMove;
      wheels[i].style.transform = 'translateZ(-82vmax) rotateX(' + wheels[i].rotation + 'deg)';
      var currentEmojiIndex = Math.abs(Math.round((wheels[i].rotation % 360) / state.slideAngle));
      if(currentEmojiIndex === state.slideCount) currentEmojiIndex = 0;
      wheels[i].winner = wheels[i].childNodes[currentEmojiIndex].textContent
    }

    if(wheels[0].winner === wheels[1].winner && wheels[2].winner === wheels[0].winner){
      console.log('wehaveawinner!')
    }
  }
})
