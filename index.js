import vdux from 'vdux'
import configureStore from './lib/store'
import app from './lib/view'

// import { DevTools, DebugPanel, LogMonitor } from 'redux-devtools/lib/react';
// import React from 'react'
// import ReactDOM from 'react-dom'

let store = configureStore()
//
// let Debug = React.createClass({
//   render() {
//     return (
//       <div>
//         <DebugPanel top right bottom>
//           <DevTools store={store} monitor={LogMonitor} />
//         </DebugPanel>
//       </div>
//     );
//   }
// })
//
// ReactDOM.render(<Debug />, document.body.lastChild);


vdux(store, app, document.body)
