import emojiRegex from 'emoji-regex'
export const SET = 'SET'
export const TOGGLE_EDITING = 'TOGGLE_EDITING'

const emojiStringToArray = function (str) {
  let split = str.match(emojiRegex());
  let arr = [];
  if(split){
    for (var i=0; i<split.length; i++) {
      let char = split[i]
      if (char !== "") {
        arr.push(char);
      }
    }
  }
  return arr;
}

export function set (e) {
  let emojis = emojiStringToArray(e.target.value)
  if(emojis.length === 7){
    return {
      emojis,
      type: SET
    }
  } else {
    return { type: SET }
  }
}

export function toggle (e) {
  return { type: TOGGLE_EDITING }
}
