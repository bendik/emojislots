export const SPIN = 'SPIN'

export function spin () {
  return {
    type: SPIN
  }
}
