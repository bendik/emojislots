import { SET } from '../actions/configure'

export const emojis = (state = ["🙂","💎","📣","🏛","🙉","👻","🍉"], action) => {
  switch (action.type) {
    case SET:
      return action.emojis || state
    default:
      return state
  }
}
