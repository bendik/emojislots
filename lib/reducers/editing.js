import { TOGGLE_EDITING } from '../actions/configure'

export const editing = (state = false, action) => {
  switch(action.type){
    case TOGGLE_EDITING:
      return !state
    default:
      return state
  }
}
