import { combineReducers } from 'redux'
import { wheels } from './wheels'
import { emojis } from './emojis'
import { editing } from './editing'
export const spread = (state = [3,3,3,3,4,4,2], action) => state
export const delay = (state = .25, action) => state
export const duration = (state = 4, action) => state

const rootReducer = combineReducers({
  wheels,
  emojis,
  spread,
  duration,
  delay,
  editing
})

export default rootReducer
