
import { SPIN } from '../actions/wheels'
import { wheel } from './wheel'

export const wheels = (state = [{rotation: 0},{rotation: 0},{rotation: 0}], action) => {
  switch (action.type) {
    case SPIN:
      return state.map(w => wheel(w, action))
    default:
      return state
  }
}
