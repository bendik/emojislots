import { SPIN } from '../actions/wheels'
import { spread } from './index'

export const wheel = (state = {rotation: 0}, action) => {
  switch (action.type) {
    case SPIN:
      let slideCount = spread().reduce((a,b) => a + b, 0)
      let degrees = Math.floor((Math.random() * slideCount) + slideCount) * (360 / slideCount)
      return { ...state, rotation: state.rotation - degrees }
    default:
      return state
  }
}
