import { createSelector } from 'reselect'

const spreadSelector = state => state.spread

export const slideCountSelector = createSelector(
  spreadSelector,
  (spread) => {
    return spread.reduce((a,b) => a + b, 0)
  }
)

export const slideAngleSelector = createSelector(
  slideCountSelector,
  (slideCount) => { return {slideAngle: 360 /slideCount }}
)
