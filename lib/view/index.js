import h from 'virtual-dom/h'
import { spin } from '../actions/wheels'
import { set, toggle } from '../actions/configure'
import { slideAngleSelector } from '../selectors/machineSelector'


export default (state) => {
  return h('slotmachine', [
    h('button.edit', {'ev-click': toggle,
      style: {
        display: (state.editing ? 'none' : 'block')
      }
    }, '🔧'),
    h('configure', {
        style: {
          display: (state.editing ? 'block' : 'none')
        }
      }, [
        h('h4', 'Customize the emojis however you like and make sure there are 7 of them'),
        h('emoji-input', [
          h('input', { 'ev-blur': set, 'value': state.emojis.join('')}),
          h('button', { 'ev-click': toggle }, 'Go Play')
        ]),
        h('p.hint', 'press command+control+spacebar to open the emoji pallette on a mac')

      ]
    ),
    h('machine', { 'ev-click': spin },
      state.wheels.map((wheel, i) => {
        return h('wheel', {
          style: {
            'transform': 'translateZ(-82vmax) rotateX(' + wheel.rotation + 'deg)',
            'transition-duration': state.duration + 's',
            'transition-delay': state.delay * i + 's'
          }},
          state.emojis.reduce((slides, emoji, j) => {
            let emojiCount = state.spread[j]
            while(emojiCount > 0){
              let slide = h('slide', {
                style: {
                  'transform': 'rotateX(' + ((360/22) * slides.length) + 'deg) translateZ(80vmax) scale(1.3)'
                }
              }, [emoji])
              slides.push(slide)
              emojiCount--
            }
            return slides
          }, [])
        )
      })
    )
  ])
}
