import { createStore, applyMiddleware, compose } from 'redux'
import rootReducer from './reducers'

import { devTools, persistState } from 'redux-devtools';

let createStoreWithMiddleware

if (process.env.NODE_ENV === 'production') {
  createStoreWithMiddleware = createStore
} else {
  createStoreWithMiddleware = compose(
    devTools(),
    persistState()
  )(createStore)
}

export default function configureStore (initialState) {
  return createStoreWithMiddleware(rootReducer, initialState)
}
